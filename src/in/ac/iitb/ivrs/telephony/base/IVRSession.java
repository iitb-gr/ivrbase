package in.ac.iitb.ivrs.telephony.base;

import in.ac.iitb.ivrs.telephony.base.events.NullEvent;

import java.lang.reflect.InvocationTargetException;
import java.util.Calendar;
import java.util.Date;

import com.continuent.tungsten.commons.patterns.fsm.Entity;
import com.continuent.tungsten.commons.patterns.fsm.Event;
import com.continuent.tungsten.commons.patterns.fsm.FiniteStateException;
import com.continuent.tungsten.commons.patterns.fsm.StateMachine;
import com.ozonetel.kookoo.Response;

/**
 * Holds data relevant to an ongoing IVR session, or a single ongoing call, between a user and the IVR system.
 */
public class IVRSession implements Entity {

	StateMachine<IVRSession> stateMachine;

	final String sessionId;
	final String userNumber;
	final String ivrNumber;
	final String circle;
	final String operator;

	int invalidTries;
	Date startTime;
	long totalCallDuration;

	Response response;

	/**
	 * Create a new IVR session.
	 * @param sessionId The KooKoo session ID for the IVR session.
	 * @param userNumber The 12-digit phone number of the user.
	 * @param ivrNumber The 12-digit IVR phone number.
	 * @param circle The cellular circle of the user.
	 * @param operator The mobile operator of the user.
	 * @param stateMachineClass The application-specific state machine class that defines the transition rules. 
	 * @throws FiniteStateException 
	 * @throws InstantiationException 
	 */
	public IVRSession(String sessionId, String userNumber, String ivrNumber, String circle, String operator,
			Class<? extends StateMachine<IVRSession>> stateMachineClass) throws FiniteStateException, InstantiationException {

		try {
			this.stateMachine = stateMachineClass.getConstructor(IVRSession.class).newInstance(this);
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {

			e.printStackTrace();
			throw new InstantiationException(e.getMessage());
		}

		this.sessionId = sessionId;
		this.userNumber = userNumber;
		this.ivrNumber = ivrNumber;
		this.circle = circle;
		this.operator = operator;

		this.startTime = Calendar.getInstance().getTime();
	}

	/**
	 * Absorb an IVR event and act on it.
	 * @param event The event that was received.
	 * @throws FiniteStateException 
	 */
	public void doEvent(Event<?> event) throws FiniteStateException {
		doEvent(event, false);
	}

	/**
	 * Absorb an IVR event and act on it recursively.
	 * @param event The event to act on.
	 * @param silent Whether to silently ignore state machine errors and return when they occur.
	 * @throws FiniteStateException
	 */
	private void doEvent(Event<?> event, boolean silent) throws FiniteStateException {
		if (silent) {
			System.out.println("Forwarding state from " + stateMachine.getState().getName());
			try {
				stateMachine.applyEvent(event);
			} catch (FiniteStateException f) {
				System.out.println("Forward did not go through.");
				return;
			}

		} else {
			System.out.println("Moving from state " + stateMachine.getState().getName() + " on " + event.getClass().getName());
			stateMachine.applyEvent(event);
		}

		if (stateMachine.isEndState()) {
			System.out.println("Reached end state.");
			stateMachine = null; // release reference to stateMachine, which also holds a reference to this session
			return;
		}

		if (silent)
			System.out.println("Forwarded to " + stateMachine.getState().getName());
		else
			System.out.println("Moved to " + stateMachine.getState().getName());

		// try forwarding to next states
		doEvent(new NullEvent(), true);
	}
	
	/**
	 * Returns the unique KooKoo session ID associated with this IVR session.
	 * @return The session ID for this IVR session.
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * Returns a 12-digit phone number (e.g. 919924714989) that includes the country code, of the person on the other
	 * side of the system.
	 * @return The user's phone number.
	 */
	public String getUserNumber() {
		return userNumber;
	}

	/**
	 * Returns a 12-digit IVR phone number (e.g. 919924714989) that includes the country code, with which the user is
	 * communicating with. 
	 * @return The IVR phone number that the user is communicating with.
	 */
	public String getIvrNumber() {
		return ivrNumber;
	}

	/**
	 * The cellular circle of the user. e.g. GUJARAT, MAHARASHTRA, etc.
	 * @return The cellular circle of the user.
	 */
	public String getCircle() {
		return circle;
	}

	/**
	 * The mobile operator of the user. e.g. Idea, Vodafone, etc.
	 * @return The mobile operator of the user.
	 */
	public String getOperator() {
		return operator;
	}

	/**
	 * Returns the number of invalid attempts at input by the user.
	 * @return The current count of invalid attempts.
	 */
	public int getInvalidTries() {
		return invalidTries;
	}

	/**
	 * Add an invalid attempt to this session.
	 */
	public void addInvalidTry() {
		invalidTries += 1;
	}

	/**
	 * Returns the start time of the call as per the web application's host's clock.
	 * @return The start time of the session.
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * Returns the current duration of the call in seconds since it started.
	 * @return The current duration of the call in seconds.
	 */
	public long getCurrentDuration() {
		return (Calendar.getInstance().getTime().getTime() - startTime.getTime()) / 1000;
	}

	/**
	 * Finish an IVR session after hangup/disconnect.
	 * @param totalCallDuration The total call duration. If not greater than 0, this will be calculated as per
	 *                          {@link IVRSession#getCurrentDuration()}.
	 */
	public void finish(long totalCallDuration) {
		if (totalCallDuration > 0)
			this.totalCallDuration = totalCallDuration;
		else
			this.totalCallDuration = getCurrentDuration();
	}

	/**
	 * Get the total call duration after the session has ended.
	 * @return The total call duration if the session has ended, 0 otherwise.
	 */
	public long getTotalDuration() {
		return totalCallDuration;
	}

	/**
	 * Returns the current KooKoo response buffer.
	 * @return The current KooKoo response buffer.
	 */
	public Response getResponse() {
		if (response == null)
			response = new Response();
		return response;
	}

	/**
	 * Flushes the response buffer and returns the XML generated.
	 * @return The XML of the response that was flushed. If no response existed, null is returned.
	 */
	public String flushResponse() {
		if (response != null) {
			String xml = response.getXML();
			response = null;
			return xml;
		} else {
			return null;
		}
	}

}
