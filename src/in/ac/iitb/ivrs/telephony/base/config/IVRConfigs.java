package in.ac.iitb.ivrs.telephony.base.config;

/**
 * Holds all the IVR configuration, which should be set by the application. 
 */
public class IVRConfigs {

	/**
	 * Telephony-specific configuration.
	 */
	public static class KooKoo {
		/**
		 * KooKoo's API key, which uniquely identifies our KooKoo account.
		 */
		public static String API_KEY = null;
	}

	/**
	 * Network-related configuration.
	 */
	public static class Network {
		/**
		 * Whether or not to use an HTTP proxy to connect to the internet.
		 */
		public static boolean USE_PROXY = false;
		/**
		 * The host of the HTTP proxy to connect to, if using proxy.
		 */
		public static String PROXY_HOST = null;
		/**
		 * The port for the HTTP proxy, if using proxy.
		 */
		public static int PROXY_PORT = 0;
		/**
		 * The LDAP username for authentication, if using proxy.
		 */
		public static String PROXY_USER = null;
		/**
		 * The LDAP password for authentication, if using proxy.
		 */
		public static String PROXY_PASS = null;
	}

}
